function twoSums (nums, target) {
  let index = []
  let temp = 0
  for (let i = 0; i < nums.length; i++) {
    temp = nums[i]
    for (let j = i + 1; j < nums.length; j++) {
      if (temp + nums[j] === target) {
        index.push(i, j)
        break
      }
    }
    if (index.length) break
  }
  return index
}

console.log(twoSums([2,7,11,15], 13))
console.log(twoSums([3,2,4], 6))
console.log(twoSums([3,3], 6))