import React from 'react'

export default function Card ({ recipe }) {
  const goSite = () => {
    window.open(recipe.href)
  }

  return (
    <div className="flex-item" onClick={goSite}>
      <img src={recipe.thumbnail} alt="img-recipe" style={{ width: '200px', height: '200px'}} />
      <h2 style={{ textAlign: 'center' }}> {recipe.title} </h2>
      <p className="ingredients" style={{ fontSize: '12px' }}>ingredients</p>
      <p className="ingredients"> {recipe.ingredients} </p>
      {/* {recipe.ingredients?.split(',').map((ingredient, index) => <label className="ingredients" key={index}>{ingredient} &ensp;|&ensp;</label> )} */}
    </div>
  )
}