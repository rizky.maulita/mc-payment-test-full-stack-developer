import loader from '../assets/loading.gif'

export default function Loading () {
  return (
    <div className="img-loading">
      <img src={loader} style={{ width: '100px'}} alt="gif-loader"  />
    </div>
  )
}