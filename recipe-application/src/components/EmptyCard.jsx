import emptyImg from '../assets/empty.png'

export default function EmptyCard () {
  return (
    <div className="img-empty">
      <img src={emptyImg} style={{ width: '400px' }} alt='empty-img' />
      <h1>Oopsss....</h1>
      <h2>Sorry... No Data Recipe !!!</h2>
    </div>
  )
}