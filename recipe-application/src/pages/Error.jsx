import serverDownImg from '../assets/server_down.png'

export default function ErrorPage () {
  return (
    <div className="img-error">
      <img src={serverDownImg} alt="img-error" style={{ width: '700px' }} />
        <h1>Oopsss....</h1>
        <h2>Something Went Wrong !!!</h2>
    </div>
  )
}