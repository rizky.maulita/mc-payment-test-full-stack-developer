import React, {useState, useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {fetchRecipe} from '../store/actions/recipeActions'
import Card from '../components/Card.jsx'
import Loading from '../components/Loading.jsx'
import ErrorPage from './Error.jsx'
import EmptyCard from '../components/EmptyCard.jsx'

export default function HomePage () {
  const dispatch = useDispatch()
  const recipes = useSelector(state => state.recipeReducer.listRecipes)
  const [listRecipes, setListRecipes] = useState([])
  const loading = useSelector(state => state.recipeReducer.loading)
  const error = useSelector(state => state.recipeReducer.error)
  const [keyword, setKeyword] = useState('')

  useEffect(() => {
    dispatch(fetchRecipe())
  }, [dispatch])

  useEffect(() => {
    setListRecipes(recipes)
  }, [recipes])

  useEffect(() => {
    if (!keyword) setListRecipes(recipes)
    else {
      const filterRecipe = recipes.filter(recipe => (
        recipe.title.toLowerCase().includes(keyword.toLowerCase()) || recipe.ingredients.toLowerCase().includes(keyword.toLowerCase())
      ))
      setListRecipes(filterRecipe)
    }
  }, [recipes, keyword])
  
  const handleSearch = (e) => {
    setKeyword(e.target.value)
  }
  const refreshHome = () => {
    setKeyword('')
  }
  
  if (loading) {
    return <Loading />
  }

  if (error) {
    return <ErrorPage />
  }
  return (
    <div>
      <div>
        <ul>
          <li onClick={refreshHome}><p>RECIPES</p></li>
          <li onClick={refreshHome}><p>Home</p></li>
          <li style={{ float: 'right' }}>
            <input value={keyword} onChange={handleSearch} type="text" name="search"/>
          </li>
        </ul>
        { listRecipes.length
          ? <div className="flex-container wrap">
              {listRecipes?.map((recipe, index) => <Card key={index} recipe={recipe} /> )}
            </div>
          : <EmptyCard />
        }
      </div>
    </div>
  )
}
