const initState = {
  listRecipes: [],
  recipe: {},
  loading: false,
  error: false
}

const reducer = (state = initState, action) => {
  switch (action.type) {
    case 'recipe/setRecipes': 
      return {...state, listRecipes: action.payload}
    case 'recipe/setRecipe':
      return {...state, recipe: action.payload}
    case 'recipe/setLoading':
      return {...state, loading: action.payload}
    case 'recipe/setError':
      return {...state, error: action.payload}
    default:
      return state
  }
}

export default reducer