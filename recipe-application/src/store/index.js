import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'
import recipeReducer from './reducers/recipeReducer'

const rootReducer = combineReducers({ recipeReducer })
const store = createStore(rootReducer, applyMiddleware(thunk))

export default store