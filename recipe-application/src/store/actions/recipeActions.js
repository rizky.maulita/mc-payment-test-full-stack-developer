const baseURL = 'https://recipe-puppy.p.rapidapi.com'

const headers = {
  'x-rapidapi-key': '4c60454ce5msh43d5d18bda319f5p1b28c9jsn8a880b368d5a',
  'x-rapidapi-host': 'recipe-puppy.p.rapidapi.com',
  useQueryString: true
}

export function fetchRecipe () {
  return (dispatch, getState) => {
    dispatch({ type: 'recipe/setLoading', payload: true })
    dispatch({ type: 'recipe/setError', payload: false })
    fetch(baseURL, { headers })
      .then(response => {
        if (response.statusText === 'OK' || response.ok) return response.json()
        else throw new Error('error')
      })
      .then( data => {
        setTimeout(() => {
          dispatch({ type: 'recipe/setRecipes', payload: data.results })
        }, 3000)
      })
      .catch(err => {
        dispatch({ type: 'recipe/setError', payload: true })
      })
      .finally(_ => {
        setTimeout(() => {
          dispatch({ type: 'recipe/setLoading', payload: false })
        }, 3000)
      })
  }
}