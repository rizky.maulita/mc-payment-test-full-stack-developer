// import logo from './logo.svg';
// import './App.css';
import './style.css'
import {Route, Switch} from 'react-router-dom'
import HomePage from './pages/Home.jsx'

function App() {
  return (
    <div>
      <Switch>
        <Route path="/" exact component={HomePage} />
      </Switch>
    </div>
  );
}

export default App;
